SOURCES=src README.md Report.pdf AssessmentItemCoverSheet.pdf
OUTPUT_ZIP=c3185790_assignment1.zip

all: dist

dist:
	zip -r $(OUTPUT_ZIP) $(SOURCES) -x *.user 

clean:
	rm -rf $(OUTPUT_ZIP) bin obj
