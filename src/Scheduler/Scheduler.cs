﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using COMP2240_Assign1.DataStructures;

namespace COMP2240_Assign1.Scheduler
{
	public abstract class Scheduler
    {
		/// <summary>
		/// Initializes a new instance of the <see cref="COMP2240_Assign1.Scheduler.Scheduler"/> class.
		/// </summary>
		public Scheduler() {
			_IsRunning = false;
		}

		/// <summary>
		/// The time quantum used for RR and FB
		/// </summary>
        public const int TimeQuantum = 4;

		/// <summary>
		/// The list of completed processes.
		/// </summary>
		protected IList<Process> CompletedProcesses = new List<Process> ();

		/// <summary>
		/// The current process running.
		/// </summary>
		private Process _CurrentProcess;
		public Process CurrentProcess {
			get {
				return _CurrentProcess;
			}

			set {
				_CurrentProcess = value;

				//when we set a new process
				//output information to console
				if (CurrentProcess != null)
					Console.WriteLine ("T{0}: {1}", this.CurrentTick, this.CurrentProcess.Id);
			}
		}

		/// <summary>
		/// Gets or sets the current tick of the scheduler
		/// </summary>
		/// <value>The current tick.</value>
		public int CurrentTick {
			get;
			internal set;
		}

		/// <summary>
		/// Start the scheduler, initialising any variables to the starting state.
		/// </summary>
		public void Start() {
			_IsRunning = true;
			CurrentTick = -1;

			this.OnStart ();
		}

		private bool _IsRunning;
		/// <summary>
		/// Determines whether this instance is running.
		/// </summary>
		/// <returns><c>true</c> if this instance is running; otherwise, <c>false</c>.</returns>
		public bool IsRunning() {
			return this._IsRunning;
		}

		/// <summary>
		/// Moves the scheduler to the next tick
		/// and adds any new given processes
		/// </summary>
		/// <param name="newProcesses">New processes.</param>
		public void Tick(IEnumerable<Process> newProcesses = null) {
			++CurrentTick;

			//process any incoming processes
			this.ProcessIncoming (newProcesses);

			//do any work that is needed this tick
			this.OnTick ();
		}

		/// <summary>
		/// Stop the scheduler
		/// </summary>
		public void Stop() {
			//set running to false
			this._IsRunning = false;

			OnStop ();
		}

		/// <summary>
		/// Get the number of completed processes.
		/// </summary>
		/// <returns>The count</returns>
		public int CompletedCount() {
			return CompletedProcesses.Count;
		}

		/// <summary>
		/// Summary report for the scheduler
		/// </summary>
		public String Report ()
		{
			var sb = new StringBuilder ();

			//headers
			sb.Append ("Process\tWaiting Time\tTurnaround Time\r\n");

			//process information
			foreach (var p in CompletedProcesses.OrderBy (p => p.Id)) {
				sb.AppendFormat ("{0}\t{1}\t\t{2}\r\n", p.Id, p.WaitingTime, p.TurnaroundTime);
			}
			sb.Append ("\r\n");

			return sb.ToString ();
		}

		/// <summary>
		/// Ends the process, doing any required clean-up
		/// </summary>
		/// <param name="process">The process to end.</param>
		protected void EndProcess(Process process) {
			//set the process turnaround time
			process.TurnaroundTime = CurrentTick - process.Arrive;

			//set the process waiting time
			process.WaitingTime = CurrentTick - process.ExecSize - process.Arrive;

			//add to completed processes
			CompletedProcesses.Add (process);
		}

		/// <summary>
		/// Averages the waiting time for all completed processes.
		/// </summary>
		/// <returns>The waiting time.</returns>
		public double AverageWaitingTime() {
			return CompletedProcesses.Average (p => p.WaitingTime);
		}

		/// <summary>
		/// Averages the turnaround time for all completed processes.
		/// </summary>
		/// <returns>The turnaround time.</returns>
		public double AverageTurnaroundTime() {
			return CompletedProcesses.Average (p => p.TurnaroundTime);
		}

		/// <summary>
		///	Process any incoming processes
		/// </summary>
		/// <param name="newProcesses">New processes.</param>
		internal abstract void ProcessIncoming(IEnumerable<Process> newProcesses = null);

		/// <summary>
		/// Raises the tick event
		/// </summary>
		internal abstract void OnTick ();

		/// <summary>
		/// Raises the start event.
		/// </summary>
		internal abstract void OnStart ();

		/// <summary>
		/// Raises the stop event.
		/// </summary>
		internal abstract void OnStop ();
    }
}
