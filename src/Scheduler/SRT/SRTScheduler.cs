﻿using System;
using System.Linq;
using System.Collections.Generic;
using COMP2240_Assign1.DataStructures;

namespace COMP2240_Assign1.Scheduler.SRT
{
	public class SRTScheduler : Scheduler
	{
		private PriorityQueue<Process> Ready;

		public SRTScheduler ()
		{
			Ready = new PriorityQueue<Process> (new SRTProcessComparer());
		}

		#region implemented abstract members of Scheduler

		internal override void ProcessIncoming (IEnumerable<Process> newProcesses = null)
		{
			foreach (var item in newProcesses) {
				Ready.Enqueue (item);
			}
		}

		internal override void OnTick ()
		{
			//if a process is running
			if (CurrentProcess != null) {
				//increment time running
				CurrentProcess.TimeRunning++;

				//process finished?
				if (CurrentProcess.TimeRunning == CurrentProcess.ExecSize) {
					EndProcess (CurrentProcess);

					CurrentProcess = null;
				}
			}

			//get a new process if nothing is running
			if (CurrentProcess == null) {
				CurrentProcess = Ready.Dequeue ();
			}

			//if a new process arrived that will finish
			//before the current process
			var p = Ready.Dequeue ();
			if (p != null) {
				if (CurrentProcess.TimeToFinish () > p.TimeToFinish ()) {
					//queue the current process
					Ready.Enqueue (CurrentProcess);

					//run the new process
					CurrentProcess = p;
				} else {
					//requeue the new process
					Ready.Enqueue (p);
				}
			}

		}

		internal override void OnStart ()
		{
			Console.WriteLine ("SRT:");
		}

		internal override void OnStop ()
		{
			Console.WriteLine ();
		}

		#endregion

		public override String ToString() {
			return "SRT";
		}
	}
}

