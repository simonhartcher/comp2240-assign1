﻿using System;
using System.Collections.Generic;

namespace COMP2240_Assign1.Scheduler.SRT
{
	public class SRTProcessComparer : IComparer<Process>
	{
		#region IComparer implementation
		/// <summary>
		/// Compare two processes for the SRT scheduling algorithm.
		/// Prefers the shortest of the two processes, or the oldest.
		/// </summary>
		/// <param name="x">The x coordinate.</param>
		/// <param name="y">The y coordinate.</param>
		public int Compare (Process x, Process y)
		{
			return x.ExecSize < y.ExecSize ? -1
					: x.ExecSize > y.ExecSize ? 1
					: x.CompareTo (y);
		}
		#endregion
	}
}

