﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace COMP2240_Assign1.Scheduler.FCFS
{
	public class FCFSScheduler : Scheduler
    {
		//fcfs ready queue
        private Queue<Process> Ready;

		public FCFSScheduler() {
			//init
			Ready = new Queue<Process> ();
		}

		internal override void ProcessIncoming(IEnumerable<Process> newProcesses = null) {
			//queue any new processes
			foreach (var item in newProcesses) {
				Ready.Enqueue (item);
			}
        }

		internal override void OnStart () {
			Console.WriteLine ("FCFS:");
		}

		internal override void OnStop ()
		{
			Console.WriteLine ();
		}

		internal override void OnTick ()
		{
			//if a process is running
			if (CurrentProcess != null) {
				//increment time running
				this.CurrentProcess.TimeRunning++;

				//process done
				if (CurrentProcess.TimeRunning == CurrentProcess.ExecSize) {
					Process p = this.CurrentProcess;

					//free current process
					this.CurrentProcess = null;

					//end it
					this.EndProcess (p);
				}
			}

			//grab new process if none is running
			if (CurrentProcess == null && Ready.Any ()) {
				CurrentProcess = Ready.Dequeue ();
			}
		}

		public override String ToString() {
			return "FCFS";
		}
    }
}
