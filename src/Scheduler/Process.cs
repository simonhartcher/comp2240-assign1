﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace COMP2240_Assign1.Scheduler
{
	public class Process : IComparable, ICloneable
    {
		/// <summary>
		/// Gets or sets the process identifier.
		/// </summary>
		/// <value>The process identifier.</value>
        public string Id { get; set; }

		/// <summary>
		/// Gets or sets the arrival time.
		/// </summary>
		/// <value>The arrival time.</value>
        public int Arrive { get; set; }

		/// <summary>
		/// Gets or sets the execution size.
		/// </summary>
		/// <value>The execution size.</value>
        public int ExecSize { get; set; }

		/// <summary>
		/// Gets or sets the time running.
		/// </summary>
		/// <value>The time running.</value>
		internal int TimeRunning {
			get;
			set;
		}

		/// <summary>
		/// Gets or sets the waiting time.
		/// </summary>
		/// <value>The waiting time.</value>
		internal int WaitingTime {
			get;
			set;
		}

		/// <summary>
		/// Gets or sets the turnaround time.
		/// </summary>
		/// <value>The turnaround time.</value>
		internal int TurnaroundTime {
			get;
			set;
		}

		/// <summary>
		/// The process time to finish.
		/// </summary>
		/// <returns>The time to finish.</returns>
		public int TimeToFinish() {
			return ExecSize - TimeRunning;
		}

		#region IComparable implementation

		public int CompareTo (object obj)
		{
			if (!(obj is Process))
				return 1;

			var other = (Process)obj;
			return this.Arrive < other.Arrive ? -1
					: this.Arrive == other.Arrive ? 0
					: 1;
		}

		#endregion

		#region ICloneable implementation

		public object Clone ()
		{
			return new Process () {
				Arrive = this.Arrive,
				ExecSize = this.ExecSize,
				Id = this.Id
			};
		}

		#endregion
    }
}
