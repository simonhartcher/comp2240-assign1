﻿using System;
using System.Linq;
using System.Collections.Generic;

namespace COMP2240_Assign1.Scheduler.Feedback
{
	public class FeedbackScheduler : Scheduler
	{
		/// <summary>
		/// The priority queues.
		/// </summary>
		Queue<Process>[] PriorityQueues;

		/// <summary>
		/// The queue identifier for the current process
		/// </summary>
		private int QueueId;

		/// <summary>
		/// The maximum number of queues
		/// </summary>
		private const int Qmax = 5;

		/// <summary>
		/// The time slice left for the current process
		/// </summary>
		private int TimeSliceLeft;

		public FeedbackScheduler () {
			//init the queues
			PriorityQueues = new Queue<Process>[Qmax + 1];
			for (int i = 0; i < PriorityQueues.Length; i++) {
				PriorityQueues [i] = new Queue<Process> ();
			}
		}
		
		/// <summary>
		/// Gets the next process to run
		/// </summary>
		/// <returns>The process to run</returns>
		private KeyValuePair<int, Process> NextProcess() {
			//init
			Process p = null;
			int id = 0;

			//return the first process it finds
			for (int i = 0; i < PriorityQueues.Length; i++) {
				var queue = PriorityQueues [i];
				if (queue.Any ()) {
					p = queue.Dequeue ();
					id = i;

					break;
				}
			}

			return new KeyValuePair<int, Process> (id, p);
		}

		#region implemented abstract members of Scheduler

		internal override void ProcessIncoming (IEnumerable<Process> newProcesses = null) {
			//add new processes to the highest priority queue
			foreach (var item in newProcesses) {
				PriorityQueues.First ().Enqueue (item);
			}
		}

		internal override void OnTick () {
			//a process is running
			if (CurrentProcess != null) {
				//increment time running
				CurrentProcess.TimeRunning++;

				//decrement time slice
				TimeSliceLeft--;

				//if the process is finished, finalise it
				if (CurrentProcess.TimeToFinish () == 0) {
					EndProcess (CurrentProcess);

					CurrentProcess = null;
				} 

				//if its out of time
				if (TimeSliceLeft == 0) {
					//if there are processes waiting in any queue
					if (PriorityQueues.Any (q => q.Any ())) {
						//demote the process to a lower queue (or readd to lowest)
						PriorityQueues [Math.Min (QueueId + 1, Qmax)].Enqueue (CurrentProcess);

						CurrentProcess = null;
					} else {
						//no other processes, reset time slice
						TimeSliceLeft = TimeQuantum;
					}
				}
			}

			//get the next process to run
			if (CurrentProcess == null) {
				var kvp = this.NextProcess ();
				if (kvp.Value != null) {
					//set the queueid
					QueueId = kvp.Key;

					CurrentProcess = kvp.Value;

					//reset time slice
					TimeSliceLeft = TimeQuantum;
				}
			}
		}

		internal override void OnStart () {
			Console.WriteLine ("FB (constant):");
		}

		internal override void OnStop () {
			Console.WriteLine ();
		}

		#endregion

		public override String ToString() {
			return "FB (constant)";
		}
	}
}

