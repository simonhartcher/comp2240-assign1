﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using COMP2240_Assign1.DataStructures;

namespace COMP2240_Assign1.Scheduler
{
    public class SchedulerTester
    {
		/// <summary>
		/// The queue of scheduler types to test.
		/// </summary>
        private Queue<Type> SchedulerTypes;

		/// <summary>
		/// The processes to test.
		/// </summary>
		private readonly LinkedList<Process> Processes;

		public SchedulerTester(IEnumerable<Process> processList)
        {
            SchedulerTypes = new Queue<Type>();
			Processes = new LinkedList<Process> (processList);

			this.InitTests ();
        }

		private void InitTests() {
			//load in the types of schedulers to test
			SchedulerTypes.Enqueue (typeof(FCFS.FCFSScheduler));
			SchedulerTypes.Enqueue (typeof(RoundRobin.RoundRobinScheduler));
			SchedulerTypes.Enqueue (typeof(SRT.SRTScheduler));
			SchedulerTypes.Enqueue (typeof(Feedback.FeedbackScheduler));
		}

        public void DoTests()
        {
			//dictionary for recording averages of each scheduler
			Dictionary<String, double[]> averages = new Dictionary<String, double[]> ();
			int index = 0;

			while (SchedulerTypes.Any ())
            {
				//create an instance of the scheduler type
                Scheduler scheduler = (Scheduler)Activator.CreateInstance(SchedulerTypes.Dequeue());

				//start it
                scheduler.Start();

                while (scheduler.IsRunning())
                {
					//if we're done
					if (scheduler.CompletedCount () == Processes.Count) {
						scheduler.Stop ();
					} else {
						//get any processes that start next tick
						var starting = Processes.Where (p => p.Arrive == scheduler.CurrentTick + 1).Select (p => (Process)p.Clone ()).ToList ();

						//move to next tick
						scheduler.Tick (starting);
					}
                }

				//output the schedular summary
				Console.Write( scheduler.Report () );

				//input the scheduler averages into the dictionary
				averages.Add(scheduler.ToString(), new double[] { scheduler.AverageWaitingTime(), scheduler.AverageTurnaroundTime() });

				index++;
            }

			//output the average stats on all the schedulers
			Console.Write (this.Summary (averages));

        }

		/// <summary>
		/// A string containing the average stats for the given schedulers
		/// </summary>
		/// <param name="averages">Averages.</param>
		private String Summary(IDictionary<String, double[]> averages) {
			//header
			var sb = new StringBuilder ("Summary\r\n\t\tAverage Waiting Time\tAverage Turnaround Time\r\n");

			//values
			foreach (var summary in averages) {
				sb.AppendFormat ("{0,-16}{1,-24:0.00}{2:0.00}\r\n", summary.Key, summary.Value.First (), summary.Value.Last ());
			}

			return sb.ToString ();
		}

    }
}
