﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace COMP2240_Assign1.Scheduler.RoundRobin
{
	public class RoundRobinScheduler : Scheduler
	{
		/// <summary>
		/// The ready queue.
		/// </summary>
		private Queue<Process> Ready;

		/// <summary>
		/// The time slice left for the current process.
		/// </summary>
		private int TimeSliceLeft;

		public RoundRobinScheduler () {
			Ready = new Queue<Process> ();
		}

		#region implemented abstract members of Scheduler

		internal override void ProcessIncoming (IEnumerable<Process> newProcesses = null)
		{
			foreach (var item in newProcesses) {
				Ready.Enqueue (item);
			}
		}

		internal override void OnTick ()
		{
			if (CurrentProcess != null) {
				CurrentProcess.TimeRunning++;
				TimeSliceLeft--;

				//process completed
				if (CurrentProcess.TimeRunning == CurrentProcess.ExecSize) {
					EndProcess (CurrentProcess);

					CurrentProcess = null;
				}

				//out of time
				if (TimeSliceLeft == 0) {

					if (Ready.Any ()) {
						//requeue
						Ready.Enqueue (CurrentProcess);

						//reset to null
						CurrentProcess = null;
					} else {
						//reset the time slice as there are no processes to follow it
						TimeSliceLeft = TimeQuantum;
					}
				}
			}

			//grab a new process if none is running
			if (CurrentProcess == null && Ready.Any()) {
				CurrentProcess = Ready.Dequeue ();
				TimeSliceLeft = 4;
			}

		}

		internal override void OnStart ()
		{
			Console.WriteLine ("RR:");
		}

		internal override void OnStop ()
		{
			Console.WriteLine ();
		}

		#endregion

		public override String ToString() {
			return "RR";
		}
	}
}

