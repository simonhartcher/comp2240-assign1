﻿using COMP2240_Assign1.Scheduler;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;

namespace COMP2240_Assign1
{
    class Program
    {
		/// <summary>
		/// Regex string to generate processes from the input file
		/// </summary>
        const string ProcessRegexString = @"ID:[\W]+(?<ID>[\S]*)[\W]+Arrive:[\W]+(?<Arrive>[\d]*)[\W]+ExecSize:[\W]+(?<ExecSize>[\d]*)";

		/// <summary>
		/// Compiled regex object
		/// </summary>
        static readonly Regex ProcessRegex = new Regex (ProcessRegexString);

		/// <summary>
		/// The entry point of the program, where the program control starts and ends.
		/// </summary>
		/// <param name="args">The command-line arguments.</param>
        static void Main(string[] args)
        {
			//error checking
			if (!args.Any ()) {
				Console.Error.WriteLine ("Error: please provide an input file.");
			} else if (!File.Exists (args.First ())) {
				Console.Error.WriteLine ("Error: input file does not exist.");
			} else { //regular execution
				//load the processes from file
				var processes = LoadProcesses (args.First ());

				//init the tester object
				var tester = new SchedulerTester (processes);

				//run the tests
				tester.DoTests ();
			}
        }

		/// <summary>
		/// Loads the processes from the given input file
		/// </summary>
		/// <returns>The processes.</returns>
		/// <param name="fileName">Path to the data file.</param>
		static IEnumerable<Process> LoadProcesses(string fileName)
        {
			//get the data from the file into a string
            string data;
            using (var streamReader = new StreamReader (fileName))
            {
                data = streamReader.ReadToEnd ();
            }

			//prepare output list
            var processes = new List<Process>();

			//process the file using regex
            var matches = ProcessRegex.Matches (data);

			//turn each match into a process then insert into the list
            foreach (Match match in matches)
            {
                processes.Add(new Process
                    {
                        Id = match.Groups["ID"].Value,
                        Arrive = int.Parse (match.Groups["Arrive"].Value),
                        ExecSize = int.Parse (match.Groups["ExecSize"].Value)
                    });
            }

            return processes;
        }
    }
}
