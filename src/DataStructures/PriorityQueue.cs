﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Collections;

namespace COMP2240_Assign1.DataStructures
{
	public class PriorityQueue<T> : IEnumerable<T> where T : IComparable
	{
		LinkedList<T> Storage;
		IComparer<T> Comparer = null;

		/// <summary>
		/// Initializes a new instance of the <see cref="COMP2240_Assign1.DataStructures.PriorityQueue`1"/> class.
		/// </summary>
		public PriorityQueue () {
			Storage = new LinkedList<T>();
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="COMP2240_Assign1.DataStructures.PriorityQueue`1"/> class.
		/// </summary>
		/// <param name="comparer">Comparer used to define object priority</param>
		public PriorityQueue (IComparer<T> comparer) : this(Enumerable.Empty<T>(), comparer) { }

		/// <summary>
		/// Initializes a new instance of the <see cref="COMP2240_Assign1.DataStructures.PriorityQueue`1"/> class.
		/// </summary>
		/// <param name="collection">Collection.</param>
		/// <param name="comparer">Comparer used to define object priority</param>
		public PriorityQueue (IEnumerable<T> collection, IComparer<T> comparer) {
			this.Comparer = comparer;
			Storage = new LinkedList<T> (collection);

			this.SortQueue ();
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="COMP2240_Assign1.DataStructures.PriorityQueue`1"/> class.
		/// </summary>
		/// <param name="collection">Collection.</param>
		public PriorityQueue (IEnumerable<T> collection) : this(collection, null) { }

		/// <summary>
		/// Enqueue the specified item.
		/// </summary>
		/// <param name="item">Item.</param>
		public void Enqueue(T item) {
			//storage is empty
			if (!Storage.Any () || Compare(item, Storage.First()) == -1 ) {
				//add to front of list
				Storage.AddFirst (item);
			} else {
				//add to end of list
				Storage.AddLast (item);
			}
		}

		/// <summary>
		/// Remove the highest priority item from the Queue,
		/// then sorts the next highest priority item into the first position.
		/// </summary>
		public T Dequeue() {
			//get item from storage
			T item = Storage.FirstOrDefault ();

			//if we have one
			if (item != null) {
				//remove it
				Storage.RemoveFirst ();

				this.SortQueue ();
			}

			return item;
		}

		/// <summary>
		/// Sorts the queue.
		/// </summary>
		private void SortQueue() {
			if (this.Storage.Count == 0) {
				//nothing to do
				return;
			}

			//iterate the list and get the highest valued item
			var iter = Storage.GetEnumerator ();
			iter.MoveNext ();

			T highest = iter.Current;
			while (iter.MoveNext ()) {
				if (Compare(iter.Current, highest) == -1) {
					highest = iter.Current;
				}
			}

			if (highest != null) {
				//remove it from the list
				Storage.Remove (highest);

				//then place it first
				Storage.AddFirst (highest);
			}
		}

		/// <summary>
		/// Compare the specified a and b.
		/// </summary>
		private int Compare(T a, T b) {
			if (Comparer != null) {
				return Comparer.Compare (a, b);
			} else {
				return a.CompareTo (b);
			}
		}

		#region IEnumerable implementation
		public IEnumerator<T> GetEnumerator ()
		{
			return Storage.GetEnumerator ();
		}

		IEnumerator IEnumerable.GetEnumerator ()
		{
			return Storage.GetEnumerator ();
		}
		#endregion
	}
}

