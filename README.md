Simon Hartcher C3185790
=======================

COMP2240 Assignment 1
=====================

Environment
-----------

Visual Studio 2012
.NET 4.5
C#

Build Instructions
------------------

The project should build without any extra dependencies.
I tested it on a lab PC in ES210 without issue.

The output will be in bin/Debug (or bin/Release)